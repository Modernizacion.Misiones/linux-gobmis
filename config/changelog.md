# Registro de Cambios
Todos los cambios relevantes a este proyecto se documentarán en este archivo.

El formato está basado en [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
y este proyecto adhiere a [Semantic Versioning](http://semver.org/spec/v2.0.0.html),
específicamente la [variante usada por Rust](http://doc.crates.io/manifest.html#the-version-field).

## [1.2.0] - 2019-08-30
### Agregado
- **Compresores**: unace, rar, p7zip-rar, sharutils, uudeview, mpack,  cabextract, lunzip 
- **Utilidades**: gnome-tweaks, pluguin Gotham de Conky para mostrar la hora y estado del equipo en el escritorio


### Modificado
- Configuración personalizada de sudoers para que el usuario normal pueda realizar las siguientes acciones anteponiendo sudo:
        Gestionar paquetes con:
            dpkg
            apt
            apt-get
            aptitude
        Editar repositorios y preferencias de APT, usando nano en terminal o pluma en escritorio
        Gestionar llaves de repositorios
        Montar dispositivos
        Apagar - Suspender - Hibernar - Sustender+Hibernar

### Eliminado
- 

## [1.0.2] - 2019-08-10
### Agregado
-

### Modificado
- Se cambia el instalador del modo vivo a **Calamares**

### Eliminado
- 



## [1.0.1] - 2019-08-01
### Agregado
- Cliente de escritorio para el programa de mensajes **Telegram**
- Emulador de DOS **DOSBox** v0.74.2
- Programa de intercambio de archivos en red **Dukto R6** v.15.03.06
- Programa de respaldo de archivos **Déjà Dup** v.3.32 
- Programa de notas pata el escritorio ***Gnote** v.3.33

### Modificado
- 

### Eliminado
- Navegador de Internet **Chromium**
- **Mumble**
- **Zim Wiki** Escritorio

