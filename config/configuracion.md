**Reseña Básica**

 Carlos Brys  |  Javier Obregón (a.k.a 4rkng3l)
 

* Distribución: **GobMis v1.0 GNU/Linux**
* Base: **Debian 10.0.0 Buster**
* Escritorio predeterminado: MATE 1.20
* Gestor de inicio de sesión: LightDM (Desactivado de forma predeterminada)

* Configuración Modo Vivo
    -> Usuario predeterminado: gobmis
    -> Contraseña usuario predeterminado y de root: misiones

* **Instalación:**

    * Debe hacerse SIEMPRE con conexión a internet.

    * GobMis puede instalarse en equipos con sistema BIOS o UEFI, desde la
    opción del menú de booteo de la ISO, a través del instalador tradicional
    Debian Installer (d-i), no obstante se "recomienda" siempre probar la
    distrubición en modo vivo antes de instalar para probar la compatibilidad
    del hardware y saber de antemano que parámetros y/o dispositivos habrá
    que configurar post instalación.

* **Aplicaciones ejecutadas al inicio del escritorio:**
    *   Lanzador de aplcaciones Plank
    *   IPTux para chat interno en LAN
    *   Synapse para buscar aplicaciones rápidamente
    *   Gestor de contraseñas KeepassXC (está configurado pero NO activado)
    *   

* **Ofimática:**
    * Suite LibreOffice 6.1.5.2
    * Organizador de libros electrónicos Calibre 3.39.1
    * Syncronizador con datos en la nube de Nextcloud
    * Organizador personal OSMO 0.4.2
    * 

* **Gráfica:**
    * GIMP
    * Inskcape (complemento Sozi para armar presentaciones)
    * Gestor de webcam Cheese
    * Gestor de scaner XSane
    * Gestor de impresoras
    * 

* **Audio / Multimedia:**
    * Reproductor y organizador de audios Clementine
    * Reproductor multimedia multiformato VLC
    * Control de volumen de Pulseaudio

* **Internet:**
    * Navegador web predeterminado Firefox ESR 60.7
    * Navegador web secundario Chromium 70.0.3683.75-1
    * Cliente de correo electrónico Thunderbird 60.7.2
        * Complemento calendario Lightning
        * Complemento firma / cifrado Enigmail
    * Mensajería interna LAN IPTux
    * Mensajero Pidgin (Multiprotocolo)
    * Cliente para conferencias VoIP Mumble
    * 

* **Otros:**
    * Gestor de contraseñas KeepassXC
    * Instalación de aplicaciones vía:
        *Gnome Software
        *Gestor de paquetes Synaptic
        *Gdebi
    * Compartición de archivos para entornos mixtos vía CIFS (Samba)
    * Herramienta para backups GRSync
    * Descompresión de archivos rar (NO LIBRE)
    * Gnome Maps
    * HPLip
    * GParted
    * Planner
    * Conky
    * Stacer
    * USBGuard

    Si bien desde Debian 10 está la posibilidad de instalar usando el instalador
    universal Calamares, éste estará disponible para próximas versiones de la
    distribución.

    Se agregó una preconfiguración para DebConf y así
    reducir las preguntas del d-i (Debian Installer) al mínimo necesario.
    La preconfigiración abarca:
        *Localización: Argentina
        *Distribución de Teclado: Latinoamericano
        *Servidor espejo predeterminado: deb.debian.org
        *Instalación SIN usuario root, se agrega automáticamente al usuario que se
        instale al grupo sudo Ver:[*]
        *Detección automática de la red

    Se pregunta por:
        *Dominio: Dejar en blanco
        *Configurar un sólo usuario que tendrá la administración usando sudo
        *Particionado de disco. Se recomiendo:
            *Usar todo el disco
            *Todos los ficheros (archivos) en una partición
        *Si el sistema es UEFI instala GRUB automáticamente
        *Si el sistema es BIOS Legacy, hay que indicar el disco donde isntalar GRUB
        que en el 99% de los casos será donde se instala la distribución es decir:
            /dev/sda

--------
[*] Usar sudo de esta forma es muy común en las distribuciones actuales, pero es una práctica poco recomendada desde la seguridad ya que habilita a los usuarios "súper poderes" de forma indiscriminada.

La mejor forma de configurar sudo a través de sudoers asigando SOLO los permisos que deban tener para trabajar en el equipo.







**Repositorios agregados:**

Centro de Aplicaciones: 
deb http://packages.elementary.io/appcenter bionic main


Jitsi: 
deb https://download.jitsi.org stable/

Pantheon: 
deb http://gandalfn.ovh/debian buster-juno main contrib os-patches

