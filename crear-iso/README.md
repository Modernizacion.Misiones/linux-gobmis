El archivo de imagen ISO de esta distribución se crea usando la herramienta **live-build**.

*live-build* es un conjunto de guiones (scripts) para crear imágenes del sistema en vivo. 

La idea detrás de la construcción en vivo es un conjunto de herramientas que utiliza un directorio de configuración para automatizar y personalizar completamente todos los aspectos de la construcción de una imagen en vivo.

El archivo de configuración para la creación de la imagen ISO se puede descargar desde el repositorio del proyecto:

https://distro.misiones.gob.ar/build/


Vea la documentación del comando para usar este recurso en: https://manpages.debian.org/testing/live-build/live-build.7.en.html

