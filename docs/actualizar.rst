Mantenimiento Post-Instalación
==============================

Luego de finalizar el proceso de la instalación, es recomendable realizar algunos ajustes a las configuraciones que se realizaron luego de que se liberó la imagen ISO.

Para falicitarle la tarea de aplicar los cambios, dispone de un archivo de comandos que automatiza este preceso, el que deberá descargar y ejecutar.

Abra una terminal pulsando las teclas :guilabel:`Alt+Ctrl+T`.

Cuando más adelante el sistema le pida una contraseña, use la  que ha indicado durante la instalación.

.. code-block:: console

    cd Descargas
    wget http://distro.misiones.gob.ar/arreglos/actualiza.sh
    sudo chmod +x actualiza.sh
    ./actualiza.sh
    rm actualiza.sh
    
Este archivo de comandos actualizará las modificaciones introducidas posteriormente a la creación del archivo ISO.

Finalizado el proceso de actualización, cierre la terminal con el comando:

.. code-block:: console

    exit
