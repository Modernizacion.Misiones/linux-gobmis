Conectar a Impresoras Compartidas en Windows
============================================

En la máquina Windows:
----------------------

Abrir el Panel de Control

Seleccionar: Programas

.. image:: imagenes/conf-impre-win-1.png
      :align: center


Seleccionar: Activar o Desctivar las Características de Windows

.. image:: imagenes/conf-impre-win-2.png
      :align: center


Buscar Servicios de impresión y documentos

Expandir el árbol

Activar Servicio de impresión LPD

.. image:: imagenes/conf-impre-win-3.png
      :align: center

Aceptar y esperar a que se actualizace del sistema. Es proceso pùede demorar un tiempo.

.. image:: imagenes/conf-impre-win-4.png
      :align: center


**Compartir la impresora en la red**

Panel de Control

Hardware y Sonido

Dispositivos e impresoras

.. image:: imagenes/compatir-impresora-win-01.png
      :align: center



Seleccionar la impresora

.. image:: imagenes/compatir-impresora-win-02.png
      :align: center


Botón derecho y buscar Propiedades de impresora

.. image:: imagenes/compatir-impresora-win-03.png
      :align: center

En la pestaña Uso compartido. 

Marcar Compartir esta impresora

Recurso compartido: Asignarle un nombre corto (preferentemente en mayúsculas)

.. image:: imagenes/compatir-impresora-win-04.png
      :align: center


**Identificar el IP de la máquina Windows que comparte la impresora:**

En el botón de buscar: escribir ``CMD`` para que apareca el programa en la lista, y ejecutarlo. 

En la ventana de comandos: escribir 

.. code-block:: console

    ipconfig


Buscar la línea: *Dirección IPv4: ... 192.168.x.x*

Tomar nota de la dirección IP de la máquina

Escribir : 

.. code-block:: console

    exit

para cerrar la ventana y salir de la consola

En la máquina GobMis:
---------------------

.. image:: imagenes/impre-win-01.png
      :align: center


.. image:: imagenes/impre-win-02.png
      :align: center


.. image:: imagenes/impre-win-03.png
      :align: center


.. image:: imagenes/impre-win-04.png
      :align: center


.. image:: imagenes/impre-win-05.png
      :align: center


.. image:: imagenes/impre-win-06.png
      :align: center


.. image:: imagenes/impre-win-07.png
      :align: center


.. image:: imagenes/impre-win-08.png
      :align: center


.. image:: imagenes/impre-win-09.png
      :align: center





