GobMis GNU/Linux
=================

.. figure:: imagenes/LogoGobMisTransparente.png
    :width: 300px
    :align: center

La **Dirección de Modernización de la Gestión y Gobierno Electrónico** de la Provincia de Misiones, pone a disposición **GobMis GNU/Linux**. Una distribución del sistema operativo Devuan (Debian), especialmente diseñado para las tareas de gestión de la administración pública provincial.

**GobMis GNU/Linux** se construyó sobre la base de un sistema operativo robusto, tolerante a fallas, ampliamente utilizado en los servidores de internet, lo que garantiza la estabilidad de la ejecución de los sistemas, y en el que se basan otras distribuciones reconocidas a nivel mundial como Ubuntu o Mint.

**GobMis GNU/Linux** es un sistema seguro. No incluye telemetría que recopile y transmita la actividad del usuario a servidores empresariales, garantizando la privacidad de los datos y acciones.

**GobMis GNU/Linux** procura la soberanía tecnológica y respeta plenamente el Decreto Provincial N°1800/07, que establece la obligatoriedad de utilizar el Estándar Abierto para Documentos Ofimáticos (ODF: OpenDocument ISO/IEC 26300/06) en la Administración Pública Provincial, al incorporar LibreOffice como suite ofimática estándar.

**GobMis GNU/Linux** integra las versiones más actuales de la aplicaciones de productividad de oficina. Los programas nunca se volverán obsoletos porque dispondrá de las actualizaciones en forma permanente.

**GobMis GNU/Linux** está hecho con **Software Libre**. Puede copiarlo, distribuirlo y adaptarlo a sus necesidades. Además no debe pagar ningún costo por las aplicaciones instaladas ni por las actualiazaciones futuras.



.. toctree::
   :maxdepth: 1
   :caption: Introducción

   introduccion

.. toctree::
   :maxdepth: 1
   :caption: Requisitos
   
   requisitos
   

.. toctree::
   :maxdepth: 1
   :caption: Pre-Instalación
   
   preparativos

.. toctree::
   :maxdepth: 1
   :caption: Instalación
   
   instalacion
   

.. toctree::
   :maxdepth: 1
   :caption: Post-Instalación
   
   actualizar
   impre-win
   drivers-wifi
   cambiar-wicd-x-network-manager
   

