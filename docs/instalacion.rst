Guía de Instalación
===================

GobMis GNU/Linux se distribuye en forma de una imagen ISO (un archivo .iso) que se puede usar para hacer un DVD de arranque o una memoria USB de arranque.

Esta guía lo ayudará a descargar la imagen ISO, crear su dispositivo de arranque e instalar GobMis GNU/Linux en su computadora.

.. toctree::
   :maxdepth: 1
   :caption: Descarga, Preparación e Instalación

   descarga
   verificacion
   quemar-dvd
   grabar-pendrive
   instalar-vivo
   instalar
   actualizar  
   drivers-impresoras
   impre-win
   drivers-wifi
   codecs
   locales
   efi
   opciones_arranque
   multiarranque
   particionado
   ayuda
   comunidad
   
