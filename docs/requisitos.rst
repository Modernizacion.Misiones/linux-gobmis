Requisitos Mínimos
==================


Para probar sin instalar:
-------------------------

* Procesador de 64 bits de al menos 2 GHz.
* 2 Gb de memoria RAM.
* Pendrive USB o Lector de DVD.

Para instalar:
--------------

* Procesador de 64 bits de al menos 2 GHz.
* 1 Gb de memoria RAM.
* Pendrive USB o Lector de DVD.
* Disco rígido de al menos 80 Gb.



