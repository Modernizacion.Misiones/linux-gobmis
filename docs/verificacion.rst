Verificación de integridad
==========================

Luego de descargar el archivo ISO de la distribución y el archivo de la firma digital de la imagen, le recomendamos que realice una verificación de la integridad del archivo para prevenir problemas durante el proceso de instalación.

En Linux
--------

Para verificar la integridad de su archivo ISO local, genere su suma SHA256 y compárelo con la suma presente en el archivo ".iso.sha256".

Abra una terminal y ejecute los comandos:

.. code-block:: console

    cd ~/Descargas/
    sha256sum -c GobMis-vx.x.x.iso.sha256

>>> GobMis-vx.x.x.iso: La suma coincide

En Windows
----------

Abra una terminal (CMD.exe) y ejecute los comandos:

.. code-block:: console

    CertUtil -hashfile GobMis-vx.x.x.iso SHA256 
    ó
    FCIV -sha1 GobMis-vx.x.x.iso
    type GobMis-vx.x.x.iso.sha256
    
.. note::
    Tenga en cuenta que el proceso de verificaciòn puede durar un tiempo, dependiendo de la velocidad de la computadora y el tamaño del archivo a verificar.    
    
    
