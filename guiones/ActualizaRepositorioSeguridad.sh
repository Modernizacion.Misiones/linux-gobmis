debmirror /srv/mirror/debian-security \
 --ignore-release-gpg  \
 --nosource \
 --host=security.debian.org \
 --root=/debian-security \
 --dist=buster/updates \
 --section=main,contrib,non-free \
 --i18n \
 --arch=amd64 \
 --passive  \
 --cleanup \
 --postcleanup  \
 --method=rsync \
 --rsync-options="-aIL --partial"  \
 --progress \
